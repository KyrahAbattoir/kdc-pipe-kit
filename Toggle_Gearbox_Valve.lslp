// Shaft Valve Script
// Toggle ON/OFF (binary) version
//
// Created by Kyrah Abattoir (KDC - Kyrah Design Concept)
// https://gitlab.com/KyrahAbattoir/kdc-pipe-kit
//
// Licensed under Creative Commons Attribution Non-Commercial Share-Alike 4.0 International
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
// SPDX-License-Identifier: CC-BY-NC-SA-4.0

//Default config for Gearbox valves.
key GEAR_TEXTURE            = "9f4c16ae-e43c-f216-9050-74fcc0bda47d"; //Texture to use on the gearbox window.
vector GEAR_REPEAT          = <1,1,0>;                                //Texture repeat.
string GEARBOX_NAME         = "gearbox";                              //The name of the gearbox prim
integer GEARBOX_FACE        = 0;                                      //The face of the gearbox window.

float MOVEMENT_RATE     = .01;  //Determinates how quickly the valve reacts to the mouse.
float GEAR_TURN_RATE    = 0.25; //0.25 for a quarter turn, 1.0 for a full turn (more is also possible)
float ROTATION_SPEED    = 10;   //Determinates how fast the valve wheel will turn (independent from MOVEMENT_RATE).
float MAX_RANGE         = 3.0;  //How close avatars have to be to interact with the valve (in meter, from the wheel itself).

// If the script output is enabled, the valve will send position updates to the root prim as link_messages
// using the following format:
// integer: value from 0 (closed) to 100 (open)
//  string: VALVE_NAME
//      id: nothing
integer MESSAGE_ENABLED   = FALSE;  //Set to true to enable ReportPosition()
string  VALVE_NAME        = "";     //A unique name that identifies this valve (if you have multiple valves)

ReportPosition()
{
    llMessageLinked(LINK_ROOT,(integer)(step*100),VALVE_NAME,"");
    //Debug
    //llOwnerSay(llList2CSV([(integer)(step*100),VALVE_NAME]));
}





//Used to find the gearbox link, named GEARBOX_NAME
integer gearbox_link;
update_links()
{
    gearbox_link=0;

    integer i;
    for(i=1;i<=llGetNumberOfPrims();i++)
    {
        if(llGetLinkName(i) == GEARBOX_NAME)
        {
            gearbox_link=i;
            return;
        }
    }
}

//Linear interpolation between two vectors.
vector vLerp(vector start, vector end, float step)
{
    return start + (end - start)*step;
}

vector base;
float step;
float move;
float last_time;

default
{
    state_entry()
    {
        if(MESSAGE_ENABLED)
            ReportPosition();

        update_links();
        llTargetOmega(llRot2Up(llGetLocalRot()),0,0);
    }
    changed(integer change)
    {
        if(change & CHANGED_LINK)
            update_links();
    }
    touch_start(integer count)
    {
        //If the avatar is too far away, we reject interaction.
        if(llVecDist(llGetPos()+llGetLocalPos(),llDetectedPos(0)) > MAX_RANGE)
        {
            llRegionSayTo(llDetectedKey(0),0,"/me is too far away, I have to get closer.");
            return;
        }

        //Movement in progress, ignore clicks.
        if(move) return;

        vector axis = llRot2Up(llGetLocalRot());
        if(step == 0.0)
        {
            //If closed, begin opening.
            move = MOVEMENT_RATE;
            llSetTimerEvent(0.01);
            llTargetOmega(axis,ROTATION_SPEED,1.0);
        }
        else if(step == 1.0)
        {
            //If open, begin closing.
            move = -MOVEMENT_RATE;
            llSetTimerEvent(0.01);
            llTargetOmega(axis,-ROTATION_SPEED,1.0);
        }
    }
    timer()
    {
        step += move;
        vector axis = llRot2Up(llGetLocalRot());

        float gear;
        if(step > 1) //Valve is opened completely.
        {
            step = 1;
            gear = step * GEAR_TURN_RATE;
            gear -= (integer)gear;
            move = 0;
            llTargetOmega(axis,0,0);

            if(gearbox_link) llSetLinkPrimitiveParamsFast(gearbox_link,[PRIM_TEXTURE,GEARBOX_FACE,GEAR_TEXTURE,GEAR_REPEAT,<gear,0,0>,PI_BY_TWO]);

            llSetTimerEvent(0);

            if(MESSAGE_ENABLED)
                ReportPosition();

            return;
        }

        if(step < 0) //Valve is closed completely.
        {
            step = 0;
            gear = step * GEAR_TURN_RATE;
            gear -= (integer)gear;
            move = 0;
            llTargetOmega(axis,0,0);

            if(gearbox_link) llSetLinkPrimitiveParamsFast(gearbox_link,[PRIM_TEXTURE,GEARBOX_FACE,GEAR_TEXTURE,GEAR_REPEAT,<gear,0,0>,PI_BY_TWO]);

            llSetTimerEvent(0);

            if(MESSAGE_ENABLED)
                ReportPosition();

            return;
        }
        //Update gearbox.
        gear = step * GEAR_TURN_RATE;
        gear -= (integer)gear;

        if(gearbox_link) llSetLinkPrimitiveParamsFast(gearbox_link,[PRIM_TEXTURE,GEARBOX_FACE,GEAR_TEXTURE,GEAR_REPEAT,<gear,0,0>,PI_BY_TWO]);
    }
}
