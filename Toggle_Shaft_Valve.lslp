// Shaft Valve Script
// Toggle ON/OFF (binary) version
//
// Created by Kyrah Abattoir (KDC - Kyrah Design Concept)
// https://gitlab.com/KyrahAbattoir/kdc-pipe-kit
//
// Licensed under Creative Commons Attribution Non-Commercial Share-Alike 4.0 International
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
// SPDX-License-Identifier: CC-BY-NC-SA-4.0

//Default config for shaft valves
string SHAFT_NAME       = "shaft";      //The name of the valve shaft within the linkset.
vector CLOSED_POSITION  = <0,-0.5,0>;   //The (local) position the shaft uses when the valve is fully closed.
vector OPEN_POSITION    = <0,-0.6,0>;   //The (local) position the shaft uses when the valve is fully open.
float MOVEMENT_RATE     = .02;          //Determinates how long the valve animates (will take roughly the same time as an analog valve).
float ROTATION_SPEED    = 500;          //Determinates how fast the valve wheel will turn in relation to the shaft translation.
float MAX_RANGE         = 3.0;          //How close avatars have to be to interact with the valve (in meter, from the wheel itself).

// If the script output is enabled, the valve will send position updates to the root prim as link_messages
// using the following format:
// integer: value from 0 (closed) to 100 (open)
//  string: VALVE_NAME
//      id: nothing
integer MESSAGE_ENABLED   = FALSE;  //Set to true to enable ReportPosition()
string  VALVE_NAME        = "";     //A unique name that identifies this valve (if you have multiple valves)

ReportPosition()
{
    llMessageLinked(LINK_ROOT,(integer)(step*100),VALVE_NAME,"");
    //Debug
    //llOwnerSay(llList2CSV([(integer)(step*100),VALVE_NAME]));
}






//Used to find the shaft link, named SHAFT_NAME
integer shaft_link;
update_links()
{
    shaft_link=0;

    integer i;
    for(i=1;i<=llGetNumberOfPrims();i++)
    {
        if(llGetLinkName(i) == SHAFT_NAME)
        {
            shaft_link=i;
            return;
        }
    }
}

//Linear interpolation between two vectors.
vector vLerp(vector start, vector end, float step)
{
    return start + (end - start)*step;
}


vector base;
float step;
float move;
float last_time;

default
{
    state_entry()
    {
        if(MESSAGE_ENABLED)
            ReportPosition();

        update_links();
        llTargetOmega(llRot2Up(llGetLocalRot()),0,0);

        //This is to help configuration, it is only shown when the script boots.
        if(shaft_link)
            llOwnerSay("Shaft link '"+SHAFT_NAME+"' was found (pos: "+(string)llGetLinkPrimitiveParams(shaft_link,[PRIM_POS_LOCAL])+").");
        else
            llOwnerSay("No shaft link named: '"+SHAFT_NAME+"' was found.");
    }
    changed(integer change)
    {
        if(change & CHANGED_LINK)
            update_links();
    }
    touch_start(integer count)
    {
        //If the avatar is too far away, we reject interaction.
        if(llVecDist(llGetPos()+llGetLocalPos(),llDetectedPos(0)) > MAX_RANGE)
        {
            llRegionSayTo(llDetectedKey(0),0,"/me is too far away, I have to get closer.");
            return;
        }

        //Movement in progress, ignore clicks.
        if(move) return;

        vector axis = llRot2Up(llGetLocalRot());
        if(step == 0.0)
        {
            //If closed, begin opening.
            move = MOVEMENT_RATE;
            llSetTimerEvent(0.01);
            llTargetOmega(axis,move*ROTATION_SPEED,1.0);
        }
        else if(step == 1.0)
        {
            //If open, begin closing.
            move = -MOVEMENT_RATE;
            llSetTimerEvent(0.01);
            llTargetOmega(axis,move*ROTATION_SPEED,1.0);
        }
    }
    timer()
    {
        step += move;
        vector axis = llRot2Up(llGetLocalRot());

        if(step > 1) //Valve is fully opened.
        {
            step = 1;
            move = 0;
            llTargetOmega(axis,0,0);

            if(shaft_link) llSetLinkPrimitiveParamsFast(shaft_link,[PRIM_POS_LOCAL,OPEN_POSITION]);

            llSetTimerEvent(0);

            if(MESSAGE_ENABLED)
                ReportPosition();

            return;
        }

        if(step < 0) //Valve is fully closed.
        {
            step = 0;
            move = 0;
            llTargetOmega(axis,0,0);

            if(shaft_link) llSetLinkPrimitiveParamsFast(shaft_link,[PRIM_POS_LOCAL,CLOSED_POSITION]);

            llSetTimerEvent(0);

            if(MESSAGE_ENABLED)
                ReportPosition();

            return;
        }

        if(shaft_link) llSetLinkPrimitiveParamsFast(shaft_link,[PRIM_POS_LOCAL,vLerp(CLOSED_POSITION,OPEN_POSITION, step)]);
    }
}
