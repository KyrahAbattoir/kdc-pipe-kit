// Shaft Valve Script
// Click & drag (analog) version
//
// Created by Kyrah Abattoir (KDC - Kyrah Design Concept)
// https://gitlab.com/KyrahAbattoir/kdc-pipe-kit
//
// Licensed under Creative Commons Attribution Non-Commercial Share-Alike 4.0 International
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
// SPDX-License-Identifier: CC-BY-NC-SA-4.0

//Default config for shaft valves
string SHAFT_NAME       = "shaft";      //The name of the valve shaft within the linkset.
vector CLOSED_POSITION  = <0,-0.5,0>;   //The (local) position the shaft uses when the valve is fully closed.
vector OPEN_POSITION    = <0,-0.6,0>;   //The (local) position the shaft uses when the valve is fully open.
float MOVEMENT_RATE     = .02;          //Determinates how quickly the valve reacts to the mouse.
float ROTATION_SPEED    = 500;          //Determinates how fast the valve wheel will turn in relation to the shaft translation.
float MAX_RANGE         = 3.0;          //How close avatars have to be to interact with the valve (in meter, from the wheel itself).

// If the script output is enabled, the valve will send position updates to the root prim as link_messages
// using the following format:
// integer: value from 0 (closed) to 100 (open)
//  string: VALVE_NAME
//      id: nothing
integer MESSAGE_ENABLED   = FALSE;  //Set to true to enable ReportPosition()
string  VALVE_NAME        = "";     //A unique name that identifies this valve (if you have multiple valves)
float   MESSAGE_RATE      = 0.50;   //Minimum time (seconds) between ReportPosition().
integer MESSAGE_PRECISION = 5;      //Minimum value change between ReportPosition() (in %).

ReportPosition()
{
	llMessageLinked(LINK_ROOT,(integer)(step*100),VALVE_NAME,"");
	//Debug
	//llOwnerSay(llList2CSV([(integer)(step*100),VALVE_NAME]));
}






//Used to find the shaft link, named SHAFT_NAME
integer shaft_link;
update_links()
{
    shaft_link=0;

    integer i;
    for(i=1;i<=llGetNumberOfPrims();i++)
    {
        if(llGetLinkName(i) == SHAFT_NAME)
        {
            shaft_link=i;
            return;
        }
    }
}

//Linear interpolation between two vectors.
vector vLerp(vector start, vector end, float step)
{
    return start + (end - start)*step;
}

vector base;
float step;
float old_step;
float last_time;
Report(integer force)
{
    if(!MESSAGE_ENABLED) return;

	if(!force)
    {
        if(llGetTime() < (last_time + MESSAGE_RATE)) return;
        if((llFabs(old_step - step)*100) < MESSAGE_PRECISION ) return;
    }
    last_time = llGetTime();
	old_step = step;
    ReportPosition();
}

key wheel_user;
float total_movement; //this is used to know if the user click&dragged.
default
{
    state_entry()
    {
        if(MESSAGE_ENABLED)
            ReportPosition();

        update_links();
        llTargetOmega(llRot2Up(llGetLocalRot()),0,0);

        //This is to help configuration, it is only shown when the script boots.
        if(shaft_link)
            llOwnerSay("Shaft link '"+SHAFT_NAME+"' was found (pos: "+(string)llGetLinkPrimitiveParams(shaft_link,[PRIM_POS_LOCAL])+").");
        else
            llOwnerSay("No shaft link named: '"+SHAFT_NAME+"' was found.");
    }
    changed(integer change)
    {
        if(change & CHANGED_LINK)
            update_links();
    }
    touch_start(integer count)
    {
        //If the avatar is too far away, we reject interaction.
        if(llVecDist(llGetPos()+llGetLocalPos(),llDetectedPos(0)) > MAX_RANGE)
        {
            llRegionSayTo(llDetectedKey(0),0,"/me is too far away, I have to get closer.");
            return;
        }

        //We only track the mouse of a single user, so first user that clicks the wheel "locks" it.
        if(wheel_user)
        {
            if(llKey2Name(wheel_user) != "") //Safety just in case user doesn't release the wheel before leaving.
                return;
        }
        wheel_user = llDetectedKey(0);
        total_movement = 0;
    }
    touch(integer count)
    {
        if(wheel_user == NULL_KEY) return;

        integer user_index = -1;
        while(count-- && user_index == -1)
        {
            if(llDetectedKey(count) == wheel_user)
                user_index = count;
        }
        if(user_index == -1) return; //In the case of multiple users, we only process "our" user.

        llSleep(0.001); //Tiny slowdown.

        //This gives us a reference point to know which way the user is pulling their mouse.
        if(base == ZERO_VECTOR)
        {
            if(llDetectedGrab(user_index) != ZERO_VECTOR)
                base = llDetectedGrab(user_index);
            return;
        }
        vector v = llDetectedGrab(user_index)-base;
        total_movement += llFabs(v.x); //accumulate to see if the wheel is being dragged at all.

        float move = v.x*0.5;
        if(move > 1)
            move = 1;
        if(move < -1)
            move=-1;
        move*=MOVEMENT_RATE;

        //Do not process "new" movement if we are already at a hard stop.
        if(step == 1.0 && move > 0) return;
        if(step == 0.0 && move < 0) return;

        vector axis = llRot2Up(llGetLocalRot());
        step += move;

        if(step > 1.0) //Valve is opened completely.
        {
            step = 1.0;

            if(shaft_link) llSetLinkPrimitiveParamsFast(shaft_link,[PRIM_POS_LOCAL,OPEN_POSITION]);

            llTargetOmega(axis,0,0);
            Report(TRUE);
            return;
        }
        else if(step < 0) //Valve is closed completely.
        {
            step = 0.0;

            if(shaft_link) llSetLinkPrimitiveParamsFast(shaft_link,[PRIM_POS_LOCAL,CLOSED_POSITION]);

            llTargetOmega(axis,0,0);
            Report(TRUE);
            return;
        }

        //Update shaft & wheel speed, report new position.
        if(shaft_link) llSetLinkPrimitiveParamsFast(shaft_link,[PRIM_POS_LOCAL,vLerp(CLOSED_POSITION,OPEN_POSITION, step)]);

        llTargetOmega(axis,move*ROTATION_SPEED,1.0);
        Report(FALSE);
    }
    touch_end(integer count)
    {
        if(wheel_user == NULL_KEY) return;

        //Release the wheel 'lock' when our registered user stops touching it.
        while(count--)
        {
            if(llDetectedKey(count) == wheel_user)
            {
                if(total_movement < 0.1) //If the user hasn't moved the wheel, we assume they don't know that you can.
                    llRegionSayTo(wheel_user,0,"/me is operated by ← clicking and dragging → the mouse.");

                wheel_user = NULL_KEY;
                llTargetOmega(llRot2Up(llGetLocalRot()),0,0);
                base = ZERO_VECTOR;
                Report(TRUE);
            }
        }
    }
}
