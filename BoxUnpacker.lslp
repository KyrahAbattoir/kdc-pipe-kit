// Box Unpacker
//
// Created by Kyrah Abattoir (KDC - Kyrah Design Concept)
// https://gitlab.com/KyrahAbattoir/kdc-pipe-kit
//
// Licensed under Creative Commons Attribution Non-Commercial Share-Alike 4.0 International
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
// SPDX-License-Identifier: CC-BY-NC-SA-4.0

Text(string text, float delay)
{
    llSleep(delay);
    llSetText(text+"\n═════╤═════\n│\n│",<1,1,1>,1.0);
    llOwnerSay(text);
}
default
{
    on_rez(integer init)
    {
        llResetScript();
    }
    state_entry()
    {
        Text("Touch this box\nto unpack your purchase!",0);
    }
    touch_start(integer count)
    {
        if(llDetectedKey(0) != llGetOwner()) return;
        llOwnerSay("Be aware that you will receive several folders in a row!");

        Text("3...",2);
        Text("2..",1);
        Text("1.",1);

        list items;
        list base   = ["KDC Read me! Important!","KDC's main shop"];

        //Yes, this is a dumb way of doing this.
        Text("Unpacking...(1/9)",1);
        items  = [
            "KDC 1m s-bend (4x6)",
            "KDC 1m s-bend (3x4)",
            "KDC 1m s-bend (2x3)",
            "KDC 1m pipe 45deg(1:3)",
            "KDC 1m pipe 90deg(1:3)",
            "KDC 1m pipe 45deg(1:2.5)",
            "KDC 1m pipe 90deg(1:2.5)",
            "KDC 1m pipe 45deg(1:1.5)",
            "KDC 1m pipe 90deg(1:1.5)",
            "KDC 1m pipe 180deg(1:3)",
            "KDC 1m pipe 180deg(1:2.5)",
            "KDC 1m pipe 180deg(1:1.5)",
            "KDC 1m pipe straight (prim)",
            "KDC 1m pipe straight-open (prim)"
        ];
        llGiveInventoryList(llGetOwner(),"KDC 1m pipes",items+base);

        Text("Unpacking...(2/9)",1);
        items  = [
            "KDC 10cm s-bend (4x6)",
            "KDC 10cm s-bend (3x4)",
            "KDC 10cm s-bend (2x3)",
            "KDC 10cm pipe 90deg(1:3)",
            "KDC 10cm pipe 45deg(1:3)",
            "KDC 10cm pipe 90deg(1:2.5)",
            "KDC 10cm pipe 45deg(1:2.5)",
            "KDC 10cm pipe 90deg(1:1.5)",
            "KDC 10cm pipe 180deg(1:3)",
            "KDC 10cm pipe 180deg(1:2.5)",
            "KDC 10cm straight (prim)",
            "KDC 10cm straight-open (prim)"
        ];
        llGiveInventoryList(llGetOwner(),"KDC 10cm pipes",items+base);

        Text("Unpacking...(3/9)",1);
        items  = [
            "KDC 30cm s-bend (4x6)",
            "KDC 30cm s-bend (3x4)",
            "KDC 30cm s-bend (2x3)",
            "KDC 30cm pipe 45deg(1:3)",
            "KDC 30cm pipe 90deg(1:3)",
            "KDC 30cm pipe 45deg(1:2.5)",
            "KDC 30cm pipe 90deg(1:2.5)",
            "KDC 30cm pipe 45deg(1:1.5)",
            "KDC 30cm pipe 90deg(1:1.5)",
            "KDC 30cm pipe 180deg(1:3)",
            "KDC 30cm pipe 180deg(1:2.5)",
            "KDC 30cm pipe 180deg(1:1.5)",
            "KDC 30cm pipe straight (prim)",
            "KDC 30cm pipe straight-open (prim)"
        ];
        llGiveInventoryList(llGetOwner(),"KDC 30cm pipes",items+base);

        Text("Unpacking...(4/9)",1);
        items  = [
            "KDC 1m knife valve (gearbox)",
            "KDC 1m knife valve (simple)",
            "KDC 1m butterfly valve (gearbox)",
            "KDC 1m butterfly valve (simple)",
            "KDC 30cm globe valve (tower)",
            "KDC 30cm globe valve (simple)",
            "KDC 30cm butterfly valve (simple)",
            "KDC 30cm butterfly valve (gearbox)",
            "KDC 10cm Globe Valve (tower)",
            "KDC 10cm Globe Valve (simple)",
            "KDC 10cm Knife Valve"
        ];
        llGiveInventoryList(llGetOwner(),"KDC Analog pipe valves",items+base);

        Text("Unpacking...(5/9)",1);
        items  = [
            "KDC 1m butterfly valve (gearbox on/off)",
            "KDC 1m knife valve (gearbox on/off)",
            "KDC 1m knife valve (simple on/off)",
            "KDC 1m butterfly valve (simple on/off)",
            "KDC 30cm globe valve (tower on/off)",
            "KDC 30cm globe valve (simple on/off)",
            "KDC 30cm butterfly valve (simple on/off)",
            "KDC 30cm butterfly valve (gearbox on/off)",
            "KDC 10cm Globe Valve (tower on/off)",
            "KDC 10cm Globe Valve (simple on/off)",
            "KDC 10cm Knife Valve (on/off)"
        ];
        llGiveInventoryList(llGetOwner(),"KDC On/Off pipe valves",items+base);

        Text("Unpacking...(6/9)",1);
        items  = [
            "KDC 1m pipe flange (wide)",
            "KDC 1m pipe flange",
            "KDC 30cm pipe flange",
            "KDC 30cm pipe flange (wide)",
            "KDC 10cm Pipe Flange",
            "KDC 10cm Pipe Flange (wide)"
        ];
        llGiveInventoryList(llGetOwner(),"KDC Pipe Flanges",items+base);

        Text("Unpacking...(7/9)",1);
        items  = [
            "KDC Pipe gasket",
            "p4_beam_d",
            "KDC Bolts_normal",
            "KDC Wheel_diffuse",
            "KDC Wheel_normal",
            "KDC Wheel_specular",
            "KDC Wheel_shaft_diffuse",
            "KDC Wheel_shaft_normal",
            "KDC Wheel_shaft_specular",
            "KDC Wheel2_diffuse",
            "KDC Wheel2_normal",
            "KDC Wheel2_specular",
            "KDC Wheel3_diffuse",
            "KDC Wheel3_normal",
            "KDC Wheel3_specular",
            "Metal_DirtyOldIndustry_512_d (light)",
            "Metal_DirtyOldIndustry_512_d",
            "Metal_DirtyOldIndustry_512_n",
            "Metal_DirtyOldIndustry_512_s"
        ];
        llGiveInventoryList(llGetOwner(),"KDC Pipe kit Textures",items+base);

        Text("Unpacking...(8/9)",1);
        items  = [
            "KDC 10cm Pipe Support (shorter)",
            "KDC 10cm Pipe Support (short)",
            "KDC 10cm Pipe Support",
            "KDC 30m pipe support post",
            "KDC 30m pipe support collar (shorter)",
            "KDC 30m pipe support collar (short)",
            "KDC 30m pipe support collar",
            "KDC 1m pipe support (small)",
            "KDC 1m pipe support (tall)",
            "KDC 1m pipe support (medium)"
        ];
        llGiveInventoryList(llGetOwner(),"KDC Pipe Supports",items+base);

        Text("Unpacking...(9/9)",1);
        items  = [
            "KDC 10cm Check Valve",
            "KDC 30cm check valve",
            "KDC 1m hydraulic sewage valve"
        ];
        llGiveInventoryList(llGetOwner(),"KDC Unscripted pipe valves",items+base);

        Text("Done!",2);
        llOwnerSay("Thank you for supporting KDC. ^_^");
        llOwnerSay("Join the secondlife:///app/group/de4c7ead-1152-8ed6-3a7f-b1fbb51801ad/about for new, updates and support!");
    }
}
